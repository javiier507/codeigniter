<form action="http://virtualhost/Codeigniter/auth/login" method="post">
    <fieldset>
        <legend>Acceder</legend>
        <div class="row">
            <div class="large-12 columns">
                <label>Usuario
                    <input type="text" name="usuario" />
                </label>
            </div>
        </div>        
        <div class="row">
            <div class="large-12 columns">
                <label>Contraseña
                    <input type="password" name="contrasena" />
                </label>
            </div>
        </div>
        <button type="submit" class="button success">Crear</button>
    </fieldset>
</form>

<?php if(isset($error_login)): ?>
    <div data-alert class="alert-box alert" style="padding-bottom: 0">
        Datos Incorrecto!
    </div>
<?php endif ?>