<!doctype>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>@yield('title', 'Codeigniter')</title>
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>