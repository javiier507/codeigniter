<form action="http://virtualhost/Codeigniter/users/store" method="post">
    <fieldset>
        <legend>Crear Usuario Nuevo</legend>
        <div class="row">
            <div class="large-12 columns">
                <label>Nombre
                    <input type="text" name="nombre" value="<?php echo set_value('nombre') ?>" />
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <label>Apellido
                    <input type="text" name="apellido" value="<?php echo set_value('apellido') ?>" />
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <label>Usuario
                    <input type="text" name="usuario" value="<?php echo set_value('usuario') ?>" />
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <label>Correo
                    <input type="email" name="correo" value="<?php echo set_value('correo') ?>" />
                </label>
            </div>
        </div>
        <button type="submit" class="button success">Crear</button>
    </fieldset>
</form>

<?php if(isset($error)): ?>
    <div data-alert class="alert-box alert" style="padding-bottom: 0">
        Datos Incorrecto!
        <ul class="side-nav" style="padding-bottom: 0">
            <li><?php echo validation_errors() ?></li>
        </ul>
    </div>
<?php endif ?>