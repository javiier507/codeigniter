<?php if(!empty($user)): ?>
    <form action="http://virtualhost/Codeigniter/users/update/<?php echo $user->id_user?>" method="post">
        <fieldset>
            <legend>Actualizar Usuario</legend>
            <input type="hidden" id="id_user" value="<?php echo $user->id_user?>">
            <div class="row">
                <div class="large-12 columns">
                    <label>Nombre
                        <input type="text" name="nombre" value="<?php echo $user->firstname?>" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Apellido
                        <input type="text" name="apellido" value="<?php echo $user->lastname?>" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Usuario
                        <input type="text" name="usuario" value="<?php echo $user->username?>" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Correo
                        <input type="email" name="correo" value="<?php echo $user->email?>" />
                    </label>
                </div>
            </div>
            <button type="submit" class="button success">Actualizar</button>
            <button class="button alert" id="eliminar-usuario">Eliminar</button>
        </fieldset>
    </form>
<?php endif ?>