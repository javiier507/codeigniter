<?php if(!empty($users)): ?>
    <table style="width: 100%">
        <caption>Lista de Usuarios</caption>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Actualizar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user): ?>
                <tr>
                    <td><?php echo $user->id_user?></td>
                    <td><?php echo $user->firstname?></td>
                    <td><?php echo $user->lastname?></td>
                    <td><?php echo $user->username?></td>
                    <td><?php echo $user->email?></td>
                    <td>
                        <a href="<?php echo 'http://virtualhost/Codeigniter/users/edit/'.$user->id_user?>" class="button info" style="margin:0 auto">
                            Actualizar
                        </a>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>
