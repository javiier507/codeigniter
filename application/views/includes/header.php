<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Codeigniter</title>
    <link rel="stylesheet" href="http://virtualhost/Codeigniter/assets/css/foundation.min.css">
</head>
<body>
    <div class="contain-to-grid sticky">
        <nav class="top-bar" data-topbar role="navigation" data-options="sticky_on: large">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="http://virtualhost/Codeigniter/">Codeigniter</a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>
            <section class="top-bar-section">
                <ul class="right">
                    <li class="active">
                        <a href="http://virtualhost/Codeigniter/users/create">Nuevo</a>
                    </li>
                    <?php if(isset($this->user)): ?>
                        <li><a href="#"><?php echo $this->user ?></a></li>
                        <li><a href="http://virtualhost/Codeigniter/auth/logout">Salir</a></li>
                    <?php endif ?>    
                </ul>
            </section>
        </nav>
    </div>
    <div class="row">