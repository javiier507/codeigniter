<?php

class Users extends CI_Controller
{
    public  function  __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->library('form_validation');
        $this->logged_in = $this->session->logged_in ? TRUE : FALSE;
        $this->user = $this->session->user ? $this->session->user : null;
    }

    private function view($name, $data)
    {
        $this->load->view('includes/header');
        $this->load->view($name, $data);
        $this->load->view('includes/footer');
    }

    public function  index()
    {
        /*$this->load->library('pagination');
        $config['base_url'] = 'http://virtualhost/Codeigniter/users/index';
        $config['total_rows'] = 6;
        $config['per_page'] = 2;

        $config['full_tag_open'] = '<ul class="pagination" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul>';
        
        $config['cur_tag_open'] = '<li class="current"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li><a href="">';
        $config['first_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li><a href="">';
        $config['num_tag_close'] = '</a></li>';
        
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li><a href="">';
        $config['last_tag_close'] = '</a></li>';

        $config['next_link'] = 'Siguiente &raquo;';
        $config['next_tag_open'] = '<li><a href="">';
        $config['next_tag_close'] = '</a></li>';
        
        $config['prev_link'] = '&laquo; Anterior';
        $config['prev_tag_open'] = '<li><a href="">';
        $config['prev_tag_close'] = '</a></li>';

        $this->pagination->initialize($config);*/

        $users = $this->UserModel->leer();
        $this->view('user/index', compact('users'));
    }

    public function create()
    {
        $this->view('user/create', null);
    }

    public function store()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('nombre', 'nombre', 'required|min_length[4]');
            $this->form_validation->set_rules('apellido', 'apellido', 'required|min_length[4]');
            $this->form_validation->set_rules('usuario', 'usuario', 'required|min_length[4]');
            $this->form_validation->set_rules('correo', 'correo', 'required|valid_email');
            
            if ($this->form_validation->run() == FALSE)
            {
                $this->view('user/create', ['error' => true]);
            }
            else
            {
                $user['firstname'] = $this->input->post('nombre');
                $user['lastname'] = $this->input->post('apellido');
                $user['username'] = $this->input->post('usuario');
                $user['email'] = $this->input->post('correo');
                $user['password'] = 'secret';
                $user['image'] = 'perfil-zorro.jpg';
                $this->UserModel->crear($user);
                redirect('http://virtualhost/Codeigniter/users');    
            }
        }
    }

    public function edit($id)
    {
        if(!$this->logged_in)
            redirect('http://virtualhost/Codeigniter/auth/access');

        $user = $this->UserModel->ver($id);
        $this->view('user/edit', compact('user'));
    }

    public function update($id)
    {
        if($this->input->post())
        {
            $user['firstname'] = $this->input->post('nombre');
            $user['lastname'] = $this->input->post('apellido');
            $user['username'] = $this->input->post('usuario');
            $user['email'] = $this->input->post('correo');
            $user['password'] = 'secret';
            $user['image'] = 'perfil-zorro.jpg';
            $this->UserModel->actualizar($user, $id);
            redirect('http://virtualhost/Codeigniter/users');
        }
    }

    public function delete($id)
    {
        $this->UserModel->eliminar($id);
        echo json_encode('id : '.$id);
    }
}