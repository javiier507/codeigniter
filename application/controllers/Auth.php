<?php

class Auth extends CI_Controller
{
    public  function  __construct()
    {
        parent::__construct();
        //$this->load->model('UserModel');
        //$this->load->library('form_validation');
        $this->logged_in = $this->session->logged_in ? TRUE : FALSE;
        $this->user = $this->session->user ? $this->session->user : null;
    }

    private function view($name, $data)
    {
        $this->load->view('includes/header');
        $this->load->view($name, $data);
        $this->load->view('includes/footer');
    }

    public function access()
    {
    	if(!$this->logged_in)
    		$this->view('auth/login', null);
    	else
    		redirect('http://virtualhost/Codeigniter/users');    	
    }

    public function login()
    {
    	if($this->input->post())
    	{
    		$username = $this->input->post('usuario');
    		$password = $this->input->post('contrasena');

    		if($username=='javiier507' && $password=='secret')
    		{
    			$this->session->logged_in = TRUE;
    			$this->session->user = $username;
    			redirect('http://virtualhost/Codeigniter/users'); 
    		}
    		else
    		{
    			$this->view('auth/login', ['error_login' => true]);
    		}
    	}
    }

    public function logout()
    {
    	$this->session->logged_in = FALSE;
    	$this->session->user = null;
    	redirect('http://virtualhost/Codeigniter/users'); 	
    }
}    