<?php

class UserModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function crear($user)
    {
        $this->db->insert('user', $user);
    }

    public function leer()
    {
        $this->db->select('id_user, firstname, lastname, username, email');
        $this->db->from('user');
        $this->db->order_by('id_user', 'desc');
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function ver($id)
    {
        $this->db->select('id_user, firstname, lastname, username, email');
        $this->db->from('user');
        $this->db->where('id_user', $id);
        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }

    public function actualizar($user, $id)
    {
        $this->db->where('id_user', $id);
        $this->db->update('user', $user);
    }

    public function eliminar($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('user');
    }
}