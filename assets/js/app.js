$(document).ready(inicio);

function inicio()
{
    $('#eliminar-usuario').click(eliminar);
}

function eliminar(event)
{
    var id_user = $('#id_user').val();
    $.ajax({
        async: true,
        type: 'DELETE',
        dataType: 'JSON',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        url: 'http://virtualhost/Codeigniter/users/delete/'+id_user,
        data: {id_user:id_user},
        timeout: 4000,
        success: function(respuesta){
            console.log('success!');
            console.log(respuesta);
            setTimeout(function(){
                window.location.href = 'http://virtualhost/Codeigniter/users';
            }, 2000);
        },
        error: function(respuesta){
            console.log(respuesta);
        }
    });
    event.preventDefault();
}